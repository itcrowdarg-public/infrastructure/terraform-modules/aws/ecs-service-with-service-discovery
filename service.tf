# Gets the CURRENT task definition from AWS, reflecting anything that's been deployed
# outside of Terraform (ie. CI builds).
# source: https://github.com/hashicorp/terraform-provider-aws/issues/632#issuecomment-341127191
data "aws_ecs_task_definition" "service" {
  task_definition = "${aws_ecs_task_definition.service.family}"
}

resource "aws_ecs_service" "service" {
  name            = "${var.service}-${var.environment}"
  cluster         = var.environment
  task_definition = "${aws_ecs_task_definition.service.family}:${max("${aws_ecs_task_definition.service.revision}", "${data.aws_ecs_task_definition.service.revision}")}"
  desired_count   = 1
  network_configuration {
    security_groups = [var.cluster_host_security_group_id]
    subnets         = var.vpc.private_subnets
  }

  deployment_maximum_percent         = 100
  deployment_minimum_healthy_percent = 0

  ordered_placement_strategy {
    type  = "spread"
    field = "host"
  }

  service_registries {
    registry_arn   = aws_service_discovery_service.service.arn
    container_name = var.module_env[var.environment].container_name
  }

}
