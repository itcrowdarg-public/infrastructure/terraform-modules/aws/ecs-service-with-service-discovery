# ECS Service - Service with Service Discovery

## Usage

```terraform
module "ecs_service_redis" {
  for_each = { for i, v in var.environments : i => v }
  source   = "git::https://gitlab.com/itcrowdarg-public/infrastructure/terraform-modules/aws/ecs-service-with-service-discovery.git?ref=1.1"

  service     = "redis"
  environment = each.value.name

  module_env = {
    staging = {
      container_name   = "redis"
      container_image  = "redis:6.2"
      container_port   = 6379
      container_cpu    = 25
      container_memory = 128
    }
    production = {
      container_name   = "redis"
      container_image  = "redis:6.2"
      container_port   = 6379
      container_cpu    = 25
      container_memory = 128
    }
  }

  vpc                                         = module.vpc
  region                                      = var.region
  cluster_host_security_group_id              = module.ecs_asg.cluster_host_security_group_id
  aws_service_discovery_private_dns_namespace = module.ecs_private_dns_namespace.aws_service_discovery_private_dns_namespace
}
```