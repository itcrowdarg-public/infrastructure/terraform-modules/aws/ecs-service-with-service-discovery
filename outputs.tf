output "service_url" {
  description = "url of the service, it doesn't have protocol nor port"
  value       = "${var.service}-${var.environment}.service"
}
