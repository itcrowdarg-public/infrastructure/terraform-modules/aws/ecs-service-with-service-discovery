resource "aws_service_discovery_service" "service" {
  name     = "${var.service}-${var.environment}"
  dns_config {
    namespace_id = var.aws_service_discovery_private_dns_namespace.id

    dns_records {
      ttl  = 10
      type = "A"
    }
  }
  health_check_custom_config {
    failure_threshold = 5
  }
}
