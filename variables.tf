variable "service" {
  description = "The name of the service with auto discovery (redis, rabbitmq, etc)"
}

variable "module_env" {
  description = "The environment variables for the service"
  type        = any
}

variable "vpc" {
  description = "VPC where the ASG is created"
}

variable "region" {
  description = "AWS region"
  type        = string
}


variable "cluster_host_security_group_id" {
  description = "Cluster Hosts security group id"
}

variable "environment" {
  description = "The name of the environment"
  type        = string
}

variable "aws_service_discovery_private_dns_namespace" {
  description = "AWS Service Discovery Private DNS Namespace"
}
