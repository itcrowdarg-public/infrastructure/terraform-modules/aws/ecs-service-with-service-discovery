resource "aws_cloudwatch_log_group" "service" {
  name              = "${var.service}-${var.environment}"
  retention_in_days = 30
}
