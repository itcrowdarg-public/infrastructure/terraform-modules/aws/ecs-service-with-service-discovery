resource "aws_ecs_task_definition" "service" {
  family       = "${var.service}-${var.environment}"
  network_mode = "awsvpc"
  container_definitions = jsonencode([
    {
      name              = var.module_env[var.environment].container_name
      image             = var.module_env[var.environment].container_image
      cpu               = var.module_env[var.environment].container_cpu
      memoryReservation = var.module_env[var.environment].container_memory
      essential         = true
      portMappings = [
        {
          containerPort = var.module_env[var.environment].container_port
          hostPort      = var.module_env[var.environment].container_port
        }
      ],
      logConfiguration = {
        "logDriver" : "awslogs",
        "options" : {
          "awslogs-group" : aws_cloudwatch_log_group.service.name,
          "awslogs-region" : var.region,
          "awslogs-stream-prefix" : "ecs",
        },
      },
    }
  ])
}



